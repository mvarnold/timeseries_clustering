#! /usr/bin/env python

""" clustering_elbow.py: performs clustering and saves output to disk."""

__author__  = "Michael Arnold"
__email__   = "mvarnold@uvm.edu"

import os
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
from sklearn.neighbors import NearestNeighbors
from pca_clusters import load_data, PCA_transform

import ctypes
mkl_rt = ctypes.CDLL('libmkl_rt.so')
mkl_get_max_threads = mkl_rt.mkl_get_max_threads
def mkl_set_num_threads(cores):
        mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(cores)))

mkl_set_num_threads(20)
print(mkl_get_max_threads())


def elbow_plot(mean_line, median_line, cluster_variance, max_means, filename):
    """ Plotting for elbow plot + in cluster variances"""
    
    def cluster_median(cluster_variance, k):
        x = cluster_variance[cluster_variance[:,0]==k][:,1]
        return np.median(x)
    def cluster_mean(cluster_variance, k):
        x = cluster_variance[cluster_variance[:,0]==k][:,1]
        return np.mean(x)
    cluster_variance = np.array(cluster_variance)
    c_median = [cluster_median(cluster_variance, k) for k in range(1,max_means)]
    c_mean = [cluster_mean(cluster_variance, k) for k in range(1,max_means)]

    # plotting
    plt.figure(figsize=(12,8))
    plt.plot(cluster_variance[:,0],cluster_variance[:,1],'.',ms=3)
    plt.plot([i for i in range(1,max_means)],mean_line,label='data mean')
    plt.plot([i for i in range(1,max_means)],c_median,label='cluster median')
    plt.plot([i for i in range(1,max_means)],c_mean,label='cluster mean')
    plt.plot([i for i in range(1,max_means)],median_line,label='data median')
    plt.legend()
    plt.xlabel("K clusters")
    plt.ylabel("in cluster variance")
    plt.savefig(f"../figures/elbow/plot_{filename}.png")
    plt.savefig(f"../figures/elbow/plot_{filename}.pdf")
    plt.close()


def kmeans_elbow(filename, max_means):
    """ Compute elbow for kmeans"""
    
    epsilon = 10**(-8)
    # load data
    x1, names, dates = load_data(filename, epsilon)
    print('loaded')
    #transform data
    pca, obs_trans_ = PCA_transform(x1)

    mean_line = []
    median_line = []
    cluster_variance = []
    for n_clusters in range(1,max_means):
        kmeanModel = KMeans(n_clusters=n_clusters,n_jobs=25).fit(obs_trans_)
        print(f'clustering: k = {n_clusters} -- {100*n_clusters/max_means:0.1f}%')
        kmeanModel.fit(obs_trans_)
        min_dist = np.min(cdist(obs_trans_, kmeanModel.cluster_centers_, 'euclidean'), axis=1)

        for i in range(n_clusters):
            cluster_variance.append([n_clusters,sum(min_dist[kmeanModel.labels_ == i])/sum(kmeanModel.labels_ == i)])
        mean_line.append(sum(min_dist)/obs_trans_.shape[0])
        median_line.append(np.median(min_dist))
    
    data_dict = {'mean' : mean_line, 'median' : median_line, 'variance':cluster_variance}
    cluster_variance = np.array(cluster_variance)
    for key, value in data_dict.items():
        np.savetxt(f'../data/elbow/{key}{max_means}{filename}.sdat', value)
    
    return mean_line, median_line, cluster_variance, max_means


def load_elbow(filename, max_means):
    """ load previosly computed elbow plot data """
    data_dict = ['mean', 'median', 'variance']
    return_list = []
    for key in data_dict:
        return_list.append(np.loadtxt(f'../data/elbow/{key}{max_means}{filename}.sdat'))
    return_list.append(max_means)
    
    return tuple(return_list)


def parse_args(args):
    """ Util function to parse command-line arguments """
    parser = argparse.ArgumentParser(description='n-gram PCA')

    parser.add_argument('-f', '--filename',
                        help='which n-grams to pca on',
                        default='top1000_#',
                        type=str)
    parser.add_argument('-r', 
                        help='flag to rerun',
                        action='store_false')
    parser.add_argument('-k', '--kmeans',
                        help="number of clusters from 1 to k",
                        default=100,
                        type=int)

    return parser.parse_args(args)


def main(args=None):
    """create elbow plots"""
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    print('filename: ', args.filename)
    filename = args.filename
    # if data exists run elbow plot with data
    if os.path.isfile(f'../data/elbow/mean{args.kmeans}{filename}.sdat') and args.r:
        mean,median,va,max_k = load_elbow(filename,args.kmeans)

    # else: compute k-means for each k
    else:
        mean,median,va,max_k =  kmeans_elbow(filename, args.kmeans)
    elbow_plot(mean,median,va,max_k, filename)

if __name__ == "__main__":
    main()
