#! /usr/bin/env python

""" pca_clusters.py: creates pca transforms, performs clustering and saves output to disk."""

__author__  = "Michael Arnold"
__email__   = "mvarnold@uvm.edu"

import os
import sys
import argparse
import datetime
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.linear_model import LinearRegression
from sklearn.decomposition import PCA, IncrementalPCA
from sklearn.cluster import MiniBatchKMeans
from sklearn.manifold import TSNE

import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches

from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
from sklearn.neighbors import NearestNeighbors


def save_sparklines(x1, filename):
    """ Saves tiny pngs for html hover sparklines"""

    path = f'../hoverplots/{filename}/figures/'
    if not os.path.exists(path):
        os.makedirs(path)

    for i, x in enumerate(x1):
        if i % 100 == 0:
            print(f'{i} of {len(x1)}')
        plt.figure(figsize=(5,1))
        plt.plot(x,',')
        plt.axis('off')
        plt.ylim((-9,-1))
        plt.savefig(path+str(i)+'.png',dpi=80)
        plt.close()


def load_data(filename, epsilon):
    """ load raw data, return log10 transform and timeseries names"""

    x1 = np.loadtxt(f'../data/{filename}.dat')
    with open(f'../data/{filename}.txt','r') as f:
        names = [i.strip() for i in f.readlines()]
    x1 = np.log10(x1+epsilon)
    dates = pd.date_range(
            start=datetime.datetime(2008,9,9),
            end=datetime.datetime(2008,9,9)+datetime.timedelta(x1.shape[1]),
            freq='D'
            )
    return x1, np.array(names), dates


def PCA_transform(x1):
    """ Returns PCA representation of data """
    pca = PCA()
    obs_trans_ = pca.fit_transform(x1)
    return pca, obs_trans_


def kmeans(obs_trans_, clusters=400, num_eigen=800):
    """ Run kmeans and return labels"""
    # train Kmeans with compressed data
    kmeanModel = KMeans(n_clusters=clusters,n_jobs=40).fit(obs_trans_[:,:num_eigen])
    #neigh = NearestNeighbors(5,1)
    #neigh.fit(obs_trans_[:,:num_eigen])
    return kmeanModel.labels_.ravel()


def tSNE_cluster(obs_trans_, num_eigen):

    Y = TSNE(n_components=2,perplexity=60).fit_transform(obs_trans_[:,:num_eigen])
    return Y 


def parse_args(args):
    """ Util function to parse command-line arguments """
    parser = argparse.ArgumentParser(description='n-gram PCA')
    
    parser.add_argument('-f', '--filename',
                        help='which n-grams to pca on',
                        default='top1000_#',
                        type=str)

    return parser.parse_args(args)


def main(args=None):
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    print('filename: ', args.filename) 
    epsilon = 10**(-8)
    # load data
    x1, names, dates = load_data(args.filename, epsilon)
    print('loaded')
    #transform data
    pca, obs_trans_ = PCA_transform(x1)
    print('transformed')
    # hoverplots
    save_sparklines(x1, args.filename)
    print('sparklines finished')
    # kmeans
    labels = kmeans(obs_trans_)

    # reduce dimensionality
    num_eigen = 800
    Y = TSNE(n_components=2,perplexity=60).fit_transform(obs_trans_[:,:num_eigen])
    
    # save
    path = f'../hoverplots/{args.filename}/'
 
    np.savetxt(f'{path}Y',Y)
    np.savetxt(f'{path}names',names,fmt='%s')
    np.savetxt(f'{path}labels',labels)    

if __name__ == "__main__":
    main()
