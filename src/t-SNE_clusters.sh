#! /usr/bin/bash

python pca_clusters.py -f $1

source activate plotly
python t-SNE_interactive.py -f $1

cd ../hoverplots/$1
tar -czvf $1.tar.gz .
scp $1.tar.gz w3:www-root/hoverplots/.
ssh w3 'cd www-root/hoverplots; mkdir $1; tar -vxf $1.tar.gz $1; rm $1.tar.gz'
