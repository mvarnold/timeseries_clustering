#! /usr/bin/env python

""" t-SNE_interactive.py: creates interactive html visualization with bokeh"""

__author__  = "Michael Arnold"
__email__   = "mvarnold@uvm.edu"

import sys
import argparse
import numpy as np
import bokeh
from bokeh.models import CustomJS, CrosshairTool
from bokeh.layouts import column
from bokeh.models.widgets import TextInput
from bokeh.plotting import figure, output_file, show, ColumnDataSource
def html_format(string):
    return string.replace('#','%23')

def parse_args(args):
    """ Util function to parse command-line arguments """
    parser = argparse.ArgumentParser(description='Bluemoon Job Parser')

    parser.add_argument('-f', '--filename',
                        help='name of n-gram collection',
                        default='top1000_#',
                        type=str)
    return parser.parse_args(args)

def main(args=None):
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)
    
    filename = args.filename
    print(f'Filename: {filename}')
    path = f'../hoverplots/{filename}/'
    webpath = f'https://mvarnold.w3.uvm.edu/hoverplots/{filename}/figures/'
    Y = np.loadtxt(f'{path}Y')
    names = np.genfromtxt(f'{path}names',comments=None,dtype=str)
    labels = np.loadtxt(f'{path}labels')

    output_file(f"{path}{filename}.html")
    c = bokeh.palettes.Category20[20]
    colors = [c[int(i)%20] for i in labels]
    source = ColumnDataSource(data=dict(
        x=Y[:, 0],
        y=Y[:, 1],
        imgs=[f'{html_format(webpath)}{i}.png' for i in range(len(Y[:,0]))],
        desc=names,
        colors=colors
    ))

    TOOLTIPS = """
        <div>
            <div>
                <img
                    src="@imgs" height="50" alt="@imgs" width="250"
                    style="float: left; margin: 0px 15px 15px 0px;"
                    border="1"
                ></img>
                <span style="font-size: 20px; font-weight: bold;">@desc</span>
                <span style="font-size: 12px; color: #966;">[$index]</span>
            </div>
            <div>
                <span style="font-size: 12px;">Location</span>
                <span style="font-size: 10px; color: #696;">($x, $y)</span>
            </div>
        </div>
    """

    p = figure(plot_width=1000, plot_height=1000, tooltips=TOOLTIPS,
               title="1grams (no hashtags/handles)")
    p.circle('x', 'y', size=5, source=source, fill_color='colors')
    text_input = TextInput(value="february", title="Search:")

    show(column(text_input,p))

if __name__ == "__main__":
    main()
